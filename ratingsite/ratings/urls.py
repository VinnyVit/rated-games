
from django.urls import path
from ratings.views import homepage, rate_this, show_game, game_list



urlpatterns = [
    path('', game_list, name="game_list"),
    path('<int:id>/ratethis/', rate_this, name="rate_this"),
    path('<int:id>/', show_game, name="show_game"),
]
