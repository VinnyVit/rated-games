from django import forms
from ratings.models import Game

class GameForm(forms.ModelForm):
    class Meta:
        model = Game
        fields = [
            "title",
            "picture",
        ]
