from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from ratings.models import Game
from ratings.forms import GameForm

# Create your views here.
def homepage (request):
    return render(request, "ratings/mainpage.html")



def rate_this(request, id):
    game = Game.objects.get(id=id)
    context = {
        "game_object": game,
    }
    return render(request,"ratings/rate_this.html", context)


def show_game(request, id):
    game = Game.objects.get(id=id)
    context = {
        "game_object": game,
    }
    return render(request,"ratings/gamedetail.html", context)

def game_list(request):
    games = Game.objects.all()
    context = {
        "game_list": games,
    }
    return render(request,"ratings/mainpage.html", context)
